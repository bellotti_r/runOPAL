from setuptools import setup
# file content adapted from:
# https://stackoverflow.com/a/39811884

setup(
   name='runOPAL',
   version='1.0',
   description='Run OPAL simulations from within Python',
   author='Andreas Adelmann et al.',
   packages=['runOPAL'],  # same as name
   python_requires='>=3.6',
    install_requires=[
        'numpy>=1.17',
        'scipy>=1.3',
        'pandas>=1.0',
        'mllib @ git+https://git@gitlab.psi.ch/adelmann/mllib.git@master'
    ]

)
